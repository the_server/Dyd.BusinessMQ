﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.BusinessMQ.Common
{
    /// <summary>
    /// 业务消息相关配置
    /// </summary>
    public class BusinessMQConfig
    {
        /// <summary>
        /// 管理数据库连接字符串
        /// </summary>
        public string ManageConnectString { get; set; }
    }
}
