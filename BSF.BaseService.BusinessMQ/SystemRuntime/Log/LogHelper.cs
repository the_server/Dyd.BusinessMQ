﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BSF.Extensions;
using BSF.BaseService.DB.Dal;
using BSF.BaseService.DB.Model;
using BSF.Db;

namespace BSF.BaseService.BusinessMQ.SystemRuntime.Log
{
    public class LogHelper
    {
        public static void WriteLine(int mqpathid, string mqpath, string methodname, string info)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(ConfigHelper.LogDBConnectString))
                {
                    SqlHelper.ExcuteSql(ConfigHelper.LogDBConnectString, (c) =>
                    {
                        tb_log_dal dal = new tb_log_dal();
                        dal.Add(c, new tb_log_model() { createtime = DateTime.Now, info = info, mqpath = mqpath, mqpathid = mqpathid, methodname = methodname });
                    });

                }
            }
            catch (Exception exp)
            {
                BSF.Log.ErrorLog.Write(string.Format("BusinessMQ插入Log信息时发生错误,mqpathid:{0},mqpath:{1},methodname:{2},info:{3}", mqpathid, mqpath.NullToEmpty(), methodname.NullToEmpty(), info.NullToEmpty()), exp);
            }
            DebugHelper.WriteLine(mqpathid, mqpath, methodname, "【记录】:" + info);
        }
    }
}
